<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>

	<form action="#" method="POST">
		<div class="field_group">
			<label for="host">Host</label>
			<input type="text" id="host" name="db_host" value="localhost" required>
		</div>
		<div class="field_group">
			<label for="user">Имя пользователя</label>
			<input type="text" id="user" name="db_user" required>
		</div>
		<div class="field_group">
			<label for="base">Имя базы</label>
			<input type="text" id="base" name="db_base" required>
		</div>
		<div class="field_group">
			<label for="password">Пароль</label>
			<input type="password" id="password" name="db_password" required>
		</div>
		<div class="field_group">
			<input type="submit" value="Создать">
		</div>
	</form>

	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

	<script>
		$("form").submit(function(e){
			e.preventDefault();
			$.ajax({
				type: 'POST',
				url: 'app/install.php',
				data: $(this).serialize(),
				success: function(data) {
					if (data == 1){
						document.body.innerHTML = "";
						document.body.append('Установка выполнена успешно');
					}
					else{
						document.body.append('Установка не выполнена');
					}
				},
				error:  function(xhr, str){
					document.body.append('<p>Возникла ошибка: ' + data + '</p>');
				}
			});
		})
	</script>

</body>
</html>