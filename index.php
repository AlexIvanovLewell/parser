<?include('app/html_dom.php');?>
<?include('app/config.php');?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Парсер котировок</title>
</head>
<body>

	<?

	try {
		$mysqli = new PDO("mysql:host=$db_host;dbname=$db_base", $db_user, $db_password);
	}
	catch (PDOException $e) {
		echo "Нет соединения с базой данных";
	}

	$dateNow = (int) time();
	$timeout = (int) ($timeout * 60);

	foreach ( $mysqli->query('SELECT * FROM time') as $row )
		$timeStart = (int) $row['time'];

	if ($timeStart == NULL)
		$timeStart = 0;

	if ( $dateNow - $timeStart < $timeout ) {
		echo "Ранний запуск скрипта, попробуйте позже.";
	}
	else {

/* Очистка таблиц */
		$mysqli->query("DELETE FROM ".$db_table_info);
		$mysqli->query("DELETE FROM ".$db_table_time);
/**/
		/* запись в БД и вывод на фронт*/
		echo "<ul>";
		foreach ( $url->find('div.inline-stocks__part') as &$items ) {
			$title = $items->find('a.inline-stocks__link', 0)->plaintext;
			$value = $items->find('span.inline-stocks__value_inner', 0)->plaintext;
			$value = str_replace(",", ".",$value);
			$dynamics = $items->find('span.inline-stocks__cell_change_small', 0)->plaintext;

			$result = $mysqli->query("INSERT INTO ".$db_table_info." (title,value,dynamics) VALUES ('$title','$value','$dynamics')");

			echo "<li>" . $title . ": " . $value . " (" . $dynamics . ")";

		}
		echo "</ul>";
		if (!$result){
			echo "Информация не занесена в базу данных";
		}
		/**/

		$result = $mysqli->query("INSERT INTO ".$db_table_time." (time) VALUES ('$dateNow')"); // Запись времени выполнения скрипта

	}

	exit();

	?>

</body>
</html>